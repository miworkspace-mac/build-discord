#!/bin/bash -x

if [[ -e "/Library/LaunchAgents/edu.umich.izzy.disable-squirrel-update-check.plist" ]]; then
	launchctl unload /Library/LaunchAgents/edu.umich.izzy.disable-squirrel-update-check.plist
	launchctl unset DISABLE_UPDATE_CHECK
	rm -rf "/Library/LaunchAgents/edu.umich.izzy.disable-squirrel-update-check.plist"
	touch "/Library/Managed Installs/discord-installed"
fi

exit 0